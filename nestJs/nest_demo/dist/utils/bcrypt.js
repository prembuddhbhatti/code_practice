"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.comparePasswords = exports.encodePassword = void 0;
const bcrypt = require("bcrypt");
function encodePassword(rawPass) {
    const SALT = bcrypt.genSaltSync();
    return bcrypt.hashSync(rawPass, SALT);
}
exports.encodePassword = encodePassword;
function comparePasswords(rawPass, hash) {
    return bcrypt.compareSync(rawPass, hash);
}
exports.comparePasswords = comparePasswords;
//# sourceMappingURL=bcrypt.js.map