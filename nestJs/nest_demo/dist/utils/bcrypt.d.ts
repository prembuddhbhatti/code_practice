export declare function encodePassword(rawPass: string): string;
export declare function comparePasswords(rawPass: string, hash: string): boolean;
