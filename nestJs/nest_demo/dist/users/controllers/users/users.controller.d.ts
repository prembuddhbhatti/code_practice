import { CreateUserDto } from 'src/users/dto/CreateUser.dto';
import { UsersService } from 'src/users/services/users/users.service';
import { SerilizedUser } from 'src/users/types';
export declare class UsersController {
    private readonly userSrevice;
    constructor(userSrevice: UsersService);
    getUsers(): SerilizedUser[];
    getByUsername(useraname: string): SerilizedUser;
    getById(id: number): SerilizedUser;
    createUser(createUserDto: CreateUserDto): Promise<import("../../../typeorm").User>;
}
