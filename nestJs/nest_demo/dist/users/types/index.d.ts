export interface User {
    id: number;
    username: string;
    password: string;
}
export declare class SerilizedUser {
    id: number;
    username: string;
    password: string;
    constructor(partial: Partial<SerilizedUser>);
}
