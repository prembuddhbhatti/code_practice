"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("../../../typeorm");
const index_1 = require("../../types/index");
const bcrypt_1 = require("../../../utils/bcrypt");
const typeorm_3 = require("typeorm");
let UsersService = class UsersService {
    constructor(userRepository) {
        this.userRepository = userRepository;
        this.users = [
            { id: 1, username: 'prem', password: 'prem' },
            { id: 2, username: 'raj', password: 'raj' },
            { id: 3, username: 'het', password: 'het' },
            { id: 4, username: 'arkit', password: 'arkit' },
        ];
    }
    getUsers() {
        return this.users.map((user) => new index_1.SerilizedUser(user));
    }
    getUserByUsername(username) {
        return this.users.find((user) => user.username === username);
    }
    getUserById(id) {
        return this.users.find((user) => user.id === id);
    }
    createUser(createUserDto) {
        const password = (0, bcrypt_1.encodePassword)(createUserDto.password);
        const newUser = this.userRepository.create(Object.assign(Object.assign({}, createUserDto), { password }));
        return this.userRepository.save(newUser);
    }
    findUserByUsername(username) {
        return this.userRepository.findOne({ where: { username: username } });
    }
    findUserById(id) {
        return this.userRepository.findOne({ where: { id: id } });
    }
};
UsersService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(typeorm_2.User)),
    __metadata("design:paramtypes", [typeorm_3.Repository])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map