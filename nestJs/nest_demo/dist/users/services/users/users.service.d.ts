import { User as UserEntity } from 'src/typeorm';
import { CreateUserDto } from 'src/users/dto/CreateUser.dto';
import { User, SerilizedUser } from 'src/users/types/index';
import { Repository } from 'typeorm';
export declare class UsersService {
    private readonly userRepository;
    constructor(userRepository: Repository<UserEntity>);
    private users;
    getUsers(): SerilizedUser[];
    getUserByUsername(username: string): User;
    getUserById(id: number): User;
    createUser(createUserDto: CreateUserDto): Promise<UserEntity>;
    findUserByUsername(username: string): Promise<UserEntity>;
    findUserById(id: number): Promise<UserEntity>;
}
