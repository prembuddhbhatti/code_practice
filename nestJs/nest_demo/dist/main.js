"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const session = require("express-session");
const passport = require("passport");
const connect_typeorm_1 = require("connect-typeorm");
const typeorm_1 = require("typeorm");
const Session_1 = require("./typeorm/Session");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    const sessionRepository = (0, typeorm_1.getRepository)(Session_1.SessionEntity);
    app.setGlobalPrefix('api');
    app.use(session({
        name: 'NESTJS_DEMO',
        secret: 'SECRETSESSIONKEY',
        resave: false,
        saveUninitialized: true,
        cookie: {
            maxAge: 60000,
        },
        store: new connect_typeorm_1.TypeormStore({}).connect(sessionRepository),
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    await app.listen(3001);
}
bootstrap();
//# sourceMappingURL=main.js.map