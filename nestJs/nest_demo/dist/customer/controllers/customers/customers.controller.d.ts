import { CustomersService } from '../../services/customers/customers.service';
import { Request, Response } from 'express';
import { CreateCustomerDto } from 'src/customer/dtos/CreateCustomer.dto';
export declare class CustomersController {
    private customerSrevice;
    constructor(customerSrevice: CustomersService);
    getCustomer(id: number, req: Request, res: Response): void;
    searchCustomerByid(id: number, req: Request, res: Response): void;
    getAllCustomers(): import("../../types/Customer").Customer[];
    createCustomer(createCustomerDto: CreateCustomerDto): void;
}
