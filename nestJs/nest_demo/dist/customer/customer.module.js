"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerModule = void 0;
const common_1 = require("@nestjs/common");
const customers_controller_1 = require("./controllers/customers/customers.controller");
const validate_customer_account_middleware_1 = require("./middleware/validate-customer-account.middleware");
const validate_customer_middleware_1 = require("./middleware/validate-customer.middleware");
const customers_service_1 = require("./services/customers/customers.service");
let CustomerModule = class CustomerModule {
    configure(consumer) {
        consumer
            .apply(validate_customer_middleware_1.ValidateCustomerMiddleware, validate_customer_account_middleware_1.ValidateCustomerAccountMiddleware, (req, res, next) => {
            console.log('account mid');
            next();
        })
            .exclude({
            path: 'api/customers/create',
            method: common_1.RequestMethod.POST,
        })
            .forRoutes(customers_controller_1.CustomersController);
    }
};
CustomerModule = __decorate([
    (0, common_1.Module)({
        controllers: [customers_controller_1.CustomersController],
        providers: [customers_service_1.CustomersService],
    })
], CustomerModule);
exports.CustomerModule = CustomerModule;
//# sourceMappingURL=customer.module.js.map