export declare class createAddressDto {
    line1: string;
    line2?: string;
    zip: string;
    city: string;
    state: string;
}
