import { Customer } from 'src/customer/types/Customer';
import { CreateCustomerDto } from 'src/customer/dtos/CreateCustomer.dto';
export declare class CustomersService {
    private customers;
    findCustomerById(id: number): Customer;
    createCustomer(customerDto: CreateCustomerDto): void;
    getCustomers(): Customer[];
}
