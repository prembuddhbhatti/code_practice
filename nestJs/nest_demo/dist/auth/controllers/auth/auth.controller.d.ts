export declare class AuthController {
    login(res: any): Promise<void>;
    getAuthSession(session: Record<string, any>): Promise<Record<string, any>>;
    getAuthStatus(): Promise<void>;
}
