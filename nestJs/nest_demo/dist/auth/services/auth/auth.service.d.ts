import { UsersService } from 'src/users/services/users/users.service';
export declare class AuthService {
    private readonly usersService;
    constructor(usersService: UsersService);
    validateUser(username: string, password: string): Promise<import("../../../typeorm").User>;
}
