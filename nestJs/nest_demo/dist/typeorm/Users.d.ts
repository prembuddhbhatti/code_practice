export declare class User {
    id: number;
    username: string;
    emailAddress: string;
    password: string;
}
