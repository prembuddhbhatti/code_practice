import { User } from './Users';
import { SessionEntity } from './Session';
declare const entities: (typeof SessionEntity | typeof User)[];
export { User, SessionEntity };
export default entities;
