import { User } from './Users';
import { SessionEntity } from './Session';
const entities = [User, SessionEntity];

export { User, SessionEntity };
export default entities;
