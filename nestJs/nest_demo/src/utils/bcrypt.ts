import * as bcrypt from 'bcrypt';

export function encodePassword(rawPass: string) {
  const SALT = bcrypt.genSaltSync();
  return bcrypt.hashSync(rawPass, SALT);
}

export function comparePasswords(rawPass: string, hash: string) {
  return bcrypt.compareSync(rawPass, hash);
}
