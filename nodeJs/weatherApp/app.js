const geocode = require("./modules/geocodes");
const forecast = require("./modules/forecast");
/* const url =
	"http://api.weatherstack.com/current?access_key=e05d7ae69bbf01985b267a938ed741a3&query=Dwarka";

request({ url: url, json: true }, (error, response) => {
	//console.log(response.body.current);
	if (error) {
		console.log("conn't connect to api");
	} else if (response.body.error) {
		console.log("error:" + response.body.error.info);
	} else {
		console.log(
			`${response.body.current.weather_descriptions[0]} It is currently ${response.body.current.temperature} and it feels like ${response.body.current.feelslike}`
		);
	}
});
 */
const address = process.argv[2];
if (!address) {
	console.log("Please Provide place");
} else {
	geocode(address, (error, { lati, longi, loca } = {}) => {
		if (error) {
			return console.log(error);
		}

		forecast(lati, longi, (error, fdata) => {
			if (error) {
				return console.log(error);
			}

			console.log(loca);
			console.log(fdata);
		});
	});
}
