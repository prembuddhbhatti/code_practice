let x = "hello"; //?it will be of type string as we have assigned it
x = "hiii";

// !we cann't change it's type like js
//x = 32;

const a = "ts";
//!we cannt reassign const
//a = "klfd";

let z; //?this will be of type any
z = 123;
z = "four"; // we can assign value of any type to this variable
console.log(z);

let zn: number; //this will specify variable type without initialization
zn = 456;
//zn = "sd"; //!this will thorugh an error

const Objz = {
	// we can reassign const Objects
	a,
	foo: "foo",

	xyz: function () {
		console.log(this.a);
	},
};

/* function fn() {
	a = "asdasd";
	console.log(a);
}
 */

Objz.xyz();
console.log(Objz.a);
Objz.foo = "acb";
