// this like giving a alias to a type

type SorN = string | number; //? SorN will act like a new type now.

let nm: SorN = 23;
let sr: SorN = "asd";
//let err: SorN = number[]; //!this will give error bcz i cannt assign a array to it
