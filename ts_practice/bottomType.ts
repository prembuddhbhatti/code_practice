let nvr: never;
//nvr = "i cant hold any thing";
//! never is used when we are sure that some variable can be of fixed number of type and not other

class Car {
	drive() {
		console.log("car");
	}
}

class Truck {
	carry() {
		console.log("carrying something");
	}
}
class Boat {
	float() {
		console.log("floating");
	}
}
type Vehicle = Car | Truck | Boat; //? here a vehicle can be be a car,truck or a boat

let newVehicle: Vehicle;

if (newVehicle instanceof Car) {
	newVehicle.drive();
} else if (newVehicle instanceof Truck) {
	newVehicle.carry();
} else if (newVehicle instanceof Boat) {
	newVehicle.float();
} else {
	const neverVehicle: never = newVehicle;
}
