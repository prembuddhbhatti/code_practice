//! by default all types in object are mandatory
let objPerson: {
	name: string;
	contact: number;
};

objPerson = {
	name: "Prem",
	contact: 123,
};

console.log(objPerson);

objPerson = {
	contact: 1234,
	name: "prashil",
};
console.log(objPerson);

/* objPerson{ //!this will give an error 
    name:"xyz"
} */

let ObjCourse: {
	//! "?" is used to make properties optional
	title: string;
	id?: number;
};

ObjCourse = {
	title: "JS",
};

console.log(ObjCourse);
