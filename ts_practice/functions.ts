import { HasEmail, HasPhone } from "./unionInter";

//?normal function

function sendEmail(to: HasEmail): { recipient: string; body: string } {
	return { recipient: `${to.name}`, body: `${to.email}` };
}

//? arrow function

const sendText = (to: HasPhone): { recipient: string; body: string } => {
	return {
		//!returning an object
		recipient: `${to.name} ${to.phone}`,
		body: "text",
	};
};

let he: HasEmail = {
	name: "prem",
	email: "email",
};
console.log(sendEmail(he));

//?rest Parameters
//! after 1st 2 parameters we can pass any number of parameters. but of type string
function Getinfo(name: string, pin: number, ...extra: string[]) {
	console.log(`Name:${name}, Pin:${pin}`);
	extra.forEach((element) => {
		console.log(`extras:${element}`);
	});
}

Getinfo("Prem", 3456, "a", "b", "c");

//? default parameters

function def(name: string = "prem") {
	console.log(name);
}
def(); //this will print default values
def("abc"); // this will print abc
