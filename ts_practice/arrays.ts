let arr: number[] = [1, 2]; //?This will specify a number type array
arr.push(123);

let aa: []; //?this will be an array of type Never so we cann't assign any thing to this
//aa.push("asd"); //!this will give an error
console.log(arr);
