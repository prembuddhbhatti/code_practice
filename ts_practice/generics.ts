interface StudInfo {
	Name: string;
	id: number;
	phone: number;
}

let stud: StudInfo = {
	Name: "Prem",
	id: 32,
	phone: 123456,
};

let studList: Array<StudInfo> = [
	{ Name: "Preashil", id: 43, phone: 456879 },
	{ Name: "Raj", id: 3, phone: 46879 },
];

studList.push(stud);

function GetStuds(students: Array<StudInfo>) {
	students.forEach((element) => {
		console.log(`Name:${element.Name}\tId:${element.id}\tPhone:${element.phone}`);
	});
}

GetStuds(studList);

// const names: Array<string> = []; // string[]
// // names[0].split(' ');

// const promise: Promise<number> = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     resolve(10);
//   }, 2000);
// });

// promise.then(data => {
//   // data.split(' ');
// })

function merge<T extends object, U extends object>(objA: T, objB: U) {
	return Object.assign(objA, objB);
}

const mergedObj = merge({ name: "Max", hobbies: ["Sports"] }, { age: 30 });
console.log(mergedObj);
