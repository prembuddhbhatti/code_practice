let anytype: any;
anytype = 12;
anytype = "you can assign any type of value to it";

let uk: unknown;
uk = "same as any";

if (typeof uk === "string") {
	console.log("str");
}
if (typeof anytype === "number") {
	console.log("num");
}

anytype.xyz.bcd.ghi; // this will give undefined
//uk.xyz.any.abc; // !this will give compilatio error
