//declaring interface
interface StudentInfo {
	Name: string;
	id: number;
	phone: number;
}

//implementing interface
let student: StudentInfo = {
	Name: "Prem",
	id: 32,
	phone: 123456,
};

let studentList: StudentInfo[] = [
	{ Name: "Preashil", id: 43, phone: 456879 },
	{ Name: "Raj", id: 3, phone: 46879 },
];

studentList.push(student);

function GetStudents(students: StudentInfo[]) {
	students.forEach((element) => {
		console.log(`Name:${element.Name}\tId:${element.id}\tPhone:${element.phone}`);
	});
}

GetStudents(studentList);
