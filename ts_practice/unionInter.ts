export interface HasPhone {
	//! export is used to export module
	name: string;
	phone: number;
}

export interface HasEmail {
	name: string;
	email: string;
}

let contactInfo: HasPhone | HasEmail = {
	//? this will allow us to provide any one of the property from email and phone
	name: "Prem",
	email: "abc@g.com",
};

let Info: HasPhone & HasEmail = {
	//? in this we need to provide all value
	name: "Prem",
	phone: 123,
	email: "abc@g.com",
};
