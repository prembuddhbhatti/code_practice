//by default all class are public

class Student {
	private id: number; //we can declare variable here

	constructor(private name: string, id: number) {
		//or we can directly assign variables here
		this.id = id;
	}

	GetInfo(): string {
		return `Id:${this.id}\tName:${this.name}`;
	}
}

let std = new Student("prem", 123);
console.log(std.GetInfo());
