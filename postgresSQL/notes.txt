install:-
    sudo apt install postgresql postgresql-contrib

connect:- 
    sudo -u postgres psql

create new role:-
    sudo -u postgres createuser --interactive

change password:-
    sudo -u postgres psql postgres
    alter user postgres with password 'postgres';


************** create database **************

CREATE DATABASE myDb;

************* list all database ************

\l 

********* connect ot database *********
\c myDb;

************ Drop database ***********
DROP DATABASE myDb;

************* create table *********

CREATE TABLE COMPANY(
   ID INT PRIMARY KEY     NOT NULL,
   NAME           TEXT    NOT NULL,
   AGE            INT     NOT NULL,
   ADDRESS        CHAR(50),
   SALARY         REAL,
   JOIN_DATE	  DATE
);

CREATE TABLE DEPARTMENT(
   ID INT PRIMARY KEY      NOT NULL,
   DEPT           CHAR(50) NOT NULL,
   EMP_ID         INT      NOT NULL
);


******* drop table ********

DROP TABLE department, company;

************** Insert ****************

INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY,JOIN_DATE) VALUES (1, 'Prem', 22, 'India', 20000.00,'2020-11-15');

INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,JOIN_DATE) VALUES (2, 'Allen', 25, 'Texas', '2007-12-13');


*************** select **************

SELECT * FROM COMPANY; 

SELECT * 
FROM COMPANY 
WHERE SALARY = NULL;

SELECT * 
FROM company 
WHERE name = 'Prem';

SELECT COUNT(*) AS "RECORDS" 
FROM COMPANY;

SELECT * FROM COMPANY 
WHERE salary IS NULL;

SELECT * FROM COMPANY 
WHERE NAME LIKE 'P%';


SELECT * FROM COMPANY 
WHERE NAME LIKE 'P%' OR ADDRESS = '%India%';


************** Update *******************

UPDATE COMPANY 
SET SALARY = 1500 
WHERE SALARY IS NULL;


*************** delete ****************

DELETE FROM COMPANY     
WHERE ID = 2;


************* views ***************

CREATE VIEW COMPANY_VIEW AS
SELECT ID, NAME, AGE
FROM  COMPANY;

SELECT * FROM COMPANY_VIEW;

************* order by *******************

SELECT * FROM COMPANY 
ORDER BY SALARY ASC;

**************** group by ******************

SELECT age, SUM(SALARY) FROM COMPANY GROUP BY AGE;

**************** Having ****************

SELECT AGE,count(AGE) 
FROM COMPANY 
GROUP BY AGE 
HAVING count(AGE) >= 2;

************* Distinct ************

SELECT DISTINCT AGE 
FROM COMPANY;

************** JOIN *************

SELECT EMP_ID, NAME, DEPT 
FROM COMPANY 
INNER JOIN DEPARTMENT
        ON COMPANY.ID = DEPARTMENT.EMP_ID;


SELECT COMPANY.ID, NAME, DEPT 
FROM COMPANY 
LEFT OUTER JOIN DEPARTMENT
            ON COMPANY.ID = DEPARTMENT.EMP_ID;

************ Unioun ******************

SELECT c.ID,c.name FROM company as c
UNION
SELECT d.ID,d.dept FROM DEPARTMENT as d