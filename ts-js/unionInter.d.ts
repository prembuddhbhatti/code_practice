export interface HasPhone {
    name: string;
    phone: number;
}
export interface HasEmail {
    name: string;
    email: string;
}
