//implementing interface
let student = {
    Name: "Prem",
    id: 32,
    phone: 123456,
};
let studentList = [
    { Name: "Preashil", id: 43, phone: 456879 },
    { Name: "Raj", id: 3, phone: 46879 },
];
studentList.push(student);
function GetStudents(students) {
    students.forEach((element) => {
        console.log(`Name:${element.Name}\tId:${element.id}\tPhone:${element.phone}`);
    });
}
GetStudents(studentList);
//# sourceMappingURL=interface.js.map