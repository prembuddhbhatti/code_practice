"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//?normal function
function sendEmail(to) {
    return { recipient: `${to.name}`, body: `${to.email}` };
}
//? arrow function
const sendText = (to) => {
    return {
        //!returning an object
        recipient: `${to.name} ${to.phone}`,
        body: "text",
    };
};
let he = {
    name: "prem",
    email: "email",
};
console.log(sendEmail(he));
//?rest Parameters
//! after 1st 2 parameters we can pass any number of parameters. but of type string
function Getinfo(name, pin, ...extra) {
    console.log(`Name:${name}, Pin:${pin}`);
    extra.forEach((element) => {
        console.log(`extras:${element}`);
    });
}
Getinfo("Prem", 3456, "a", "b", "c");
//? default parameters
function def(name = "prem") {
    console.log(name);
}
def(); //this will print default values
def("abc"); // this will print abc
//# sourceMappingURL=functions.js.map