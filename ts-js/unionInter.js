"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let contactInfo = {
    //? this will allow us to provide any one of the property from email and phone
    name: "Prem",
    email: "abc@g.com",
};
let Info = {
    //? in this we need to provide all value
    name: "Prem",
    phone: 123,
    email: "abc@g.com",
};
//# sourceMappingURL=unionInter.js.map