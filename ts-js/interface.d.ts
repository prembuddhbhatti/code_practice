interface StudentInfo {
    Name: string;
    id: number;
    phone: number;
}
declare let student: StudentInfo;
declare let studentList: StudentInfo[];
declare function GetStudents(students: StudentInfo[]): void;
