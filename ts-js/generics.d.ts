interface StudInfo {
    Name: string;
    id: number;
    phone: number;
}
declare let stud: StudInfo;
declare let studList: Array<StudInfo>;
declare function GetStuds(students: Array<StudInfo>): void;
