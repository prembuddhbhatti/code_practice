//by default all class are public
class Student {
    constructor(name, id) {
        this.name = name;
        //or we can directly assign variables here
        this.id = id;
    }
    GetInfo() {
        return `Id:${this.id}\tName:${this.name}`;
    }
}
let std = new Student("prem", 123);
console.log(std.GetInfo());
//# sourceMappingURL=class.js.map